def impute_data(data_set,comp_info,app_name,app_id):
    ind=DataFrame({'Time':pd.date_range(min(data_set.Time),max(data_set.Time),freq='1min')})
    data_set=ind.merge(data_set,on='Time',how='outer',sort=True)
    data_set=data_set.fillna(method='ffill')
    data_set=data_set.fillna(method='bfill')
    data_set=data_set.fillna(0)
    if not os.path.exists(Base_Dir+'/Data/'+app_name):
        os.makedirs(Base_Dir+'/Data/'+app_name)
    else:
        shutil.rmtree(Base_Dir+'/Data/'+app_name)
        os.makedirs(Base_Dir+'/Data/'+app_name)
    data_set.to_csv(Base_Dir+'/Data/'+app_name+'/Raw_df.csv',index=False)
    comp_info.to_csv(Base_Dir+'/Data/'+app_name+'/Comp_Info.csv',index=False)
    return data_set