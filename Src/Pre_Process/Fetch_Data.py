def fetch_data(hostip,app_id,frm,to,comp_list,txn_list,txn_kpis='Total,AvgRespTime,SlowPercentage'):
    t0=time.time()
    t1 = time.time()
    dquery = ('select G.Id CompId,G.AliasName Name,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C where G.CompMetricSetId=C.Id and G.Id in (%s);' %comp_list)
    data_refresh = DataFrame()
    com_data=DataFrame()
    try:
        if(len(comp_list)>0):
            com_data = db_result(dquery)
            com_data['Layer']=len(com_data) * ['NA']
        else:
                print('No Comp Id Given')
        try:
            if(len(txn_list)>0):
                tquery = ('select distinct TxnId CompId,Name from TRANSACTIONCOLL where AppInstanceId=%s and Time between \'%s\' and \'%s\' and TxnId in (%s) order by Name'%(app_id,frm,to,txn_list))
                tco_data = db_result(tquery)
                if(len(tco_data)>0):
                    tco_data['Colltab']='TRANSACTIONCOLL'
                    tco_data['Layer']='TXN'
                    com_data=com_data.append(tco_data,ignore_index=True)
                    com_data['App_Id'] = app_id
                    print('Started Collecting Transaction Data')
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip, frm, to,app_id,'Total,AvgRespTime,SlowPercentage',txn_list)) as url:
                    #   txn_data = json.loads(url.read().decode())
                    r = requests.get("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip,frm,to,app_id,txn_kpis,txn_list))
                    txn_data=r.json()
                    if ((txn_data['responseStatus'] in ('Sucess', 'success')) & (isinstance(txn_data['result'], list)) & (len(txn_data['result'])>1)):
                        txn_data = DataFrame(txn_data['result'])
                        #txn_data = txn_data[:-1]
                        txn_data['Time'] = pd.to_datetime(txn_data['Time'], format='%Y-%m-%d %H:%M:%S')
                        #txn_data['Time']=pd.to_datetime((txn_data['Time']+ datetime.timedelta(hours=5, minutes=30)).apply(lambda x: x.strftime('%Y-%m-%d %H:%M:%S')),format='%Y-%m-%d %H:%M:%S')
                        print('Transaction Data Set %s,%s'%(txn_data.shape[0],txn_data.shape[1]))
                        data_refresh = txn_data
                        print('Completed Transaction Data')
                    else:
                        print('No Txn Data For the selected Date range')
                else:
                    print('No Txn Data For the selected Date range')
            else:
                print('No Txn Id Given')
            Layer = com_data
            if (len(comp_list) > 0):
                for i in com_data[(com_data.Layer !='TXN')].CompId:
                    colltab = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Colltab.item()
                    comp_name = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item()
                    print('Started Collecting Data For %s' % comp_name)
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab)) as url:
                    #    kpidata = json.loads(url.read().decode())
                    r=requests.get("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab))
                    kpidata=r.json()
                    if ((kpidata['responseStatus'] in ('Sucess', 'success')) & (isinstance(kpidata['result'], list)) & (len(kpidata['result'])>1)):
                        data = DataFrame(kpidata['result'])
                        #data = data[:-1]
                        print('%s Data Set %s,%s' % (comp_name,data.shape[0], data.shape[1]))
                        data['Time'] = pd.to_datetime(data['Time'], format='%Y-%m-%d %H:%M:%S')
                        Layer.set_value(Layer[(Layer.CompId == i)&(Layer.Layer !='TXN')].CompId.index.item(), 'Layer',
                                        re.sub(' ', '_', kpidata['properties']['layerName']))
                        col=['Time']
                        col.extend([col for col in data.columns if
                               col not in ['MININGCLUSTERID', 'BMININGCLUSTERID', 'TRANSACTION_CLUSTER_ID',
                                           'COMPINSTANCEID','Time']])
                        #col.extend(['Time'])
                        data = data[col]
                        cols=['Time']
                        cols.extend([com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item() + '_' + str(j) for j in data.iloc[:, data.columns != 'Time'].columns])
                        #cols.extend(['Time'])
                        data.columns=cols
                        #if ((i == com_data.CompId[0]) & (txn_list=='NULL')):
                        if(len(data_refresh)==0):
                            data_refresh = data_refresh.append(data)
                        else:
                            data_refresh = pd.merge(data_refresh, data, on='Time', how='outer',sort=True)
                        print('Collected Data For %s' % comp_name)
                    else:
                        print('No data for the selected date range')
            if(len(data_refresh)>0):
                print("Time Taken To Fetch Data: %s" % (time.time() - t1))
                t1=time.time()
                #com_data=pd.merge(com_data,Layer,how='outer',on=
                com_data=Layer
                #print("Filling Missing Values")
                #rng = pd.date_range(start=min(data_refresh.Time), periods=data_refresh.shape[0], freq=str(1) + 'min')
                #rng = pd.DataFrame(rng, columns=['Time'])
                cols = [col for col in data_refresh.columns if col not in ['Time']]
                data_refresh['Time'] = pd.DatetimeIndex(data_refresh['Time'])
                data_refresh[cols] = data_refresh[cols].apply(pd.to_numeric, errors='coerce')
                #complete_data = pd.merge(data_refresh, rng, how='outer', on=['Time'])
                #complete_data=do(complete_data,cols,5)
                #print("Time Taken To Fill Missing Values: %s" % (time.time() - t1))
        except:
            print('Check if the DataService is running')
    except:
        print('DB Connection Failed')
    print("Total Time Taken To Fetch KPI Data From DB: %s" % (time.time() - t0))
    return data_refresh,com_data