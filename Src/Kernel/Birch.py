
def birch_main(app_name,frm,to,nc=20,percentage=0.5):
    txn_imputated_data,kpi_imputated_data=pre_process(app_name,hostip,frm,to,roll_apply,ema_win_len)
    result=birch(txn_imputated_data,kpi_imputated_data,nc,percentage)    
    plot_birch(kpi_imputated_data,result,app_name,roll_apply,percentage)
    return result

def pre_process(app_name,hostip,frm,to,roll_apply,ema_win_len):
    app_id,comp_list,txn_list=comp_txn(app_list,app_name)
    txn_data_set,txn_comp_info=fetch_data(hostip,app_id,frm,to,'',txn_list,'Total')
    txn_data_set.index=pd.DatetimeIndex(txn_data_set.Time)
    txn_agg_data=non_overlapping_average(txn_data_set,roll_apply)
    txn_agg_data['Time']=txn_agg_data.index
    ind=DataFrame({'Time':pd.date_range(min(txn_agg_data.Time),max(txn_agg_data.Time),freq=str(roll_apply)+'min')})
    txn_data_set=ind.merge(txn_agg_data,on='Time',how='outer',sort=True)
    txn_imputated_data=txn_data_set[:]
    txn_cols = [col for col in txn_imputated_data.columns if col not in ['Time']]
    txn_imputated_data=impute_using_sma(txn_imputated_data,txn_cols,ema_win_len)
    
    kpi_data_set,kpi_comp_info=fetch_data(hostip,app_id,frm,to,comp_list,'')
    kpi_data_set.index=pd.DatetimeIndex(kpi_data_set.Time)
    kpi_agg_data=non_overlapping_average(kpi_data_set,roll_apply)
    kpi_agg_data['Time']=kpi_agg_data.index
    ind=DataFrame({'Time':pd.date_range(min(kpi_agg_data.Time),max(kpi_agg_data.Time),freq=str(roll_apply)+'min')})
    kpi_data_set=ind.merge(kpi_agg_data,on='Time',how='outer',sort=True)
    kpi_imputated_data=kpi_data_set[:]
    kpi_cols = [col for col in kpi_imputated_data.columns if col not in ['Time']]
    kpi_imputated_data=impute_using_sma(kpi_imputated_data,kpi_cols,ema_win_len)
    
    return txn_imputated_data,kpi_imputated_data


def birch_norm(txn_imputated_data,kpi_imputated_data):
    txn_imputated_data[txn_cols]=(txn_imputated_data[txn_cols]-txn_imputated_data[txn_cols].mean())/txn_imputated_data[txn_cols].std()
    kpi_imputated_data[kpi_cols]=(kpi_imputated_data[kpi_cols]-kpi_imputated_data[kpi_cols].mean())/kpi_imputated_data[kpi_cols].std()
    txn_imputated_data=txn_imputated_data.fillna(0)
    kpi_imputated_data=kpi_imputated_data.fillna(0)
    return txn_imputated_data,kpi_imputated_data
    
def birch(txn_imputated_data,kpi_imputated_data,nc=16,percentage=None):
    if(len(txn_imputated_data)>0):
        brc = Birch(n_clusters=nc)
        cols = [col for col in txn_imputated_data.columns if col not in ['Time']]
        if((not not percentage) & (len(kpi_imputated_data)>0)):
            train=txn_imputated_data[:round(len(txn_imputated_data)*percentage)]
            test=txn_imputated_data[round(len(txn_imputated_data)*percentage):]
            brc.fit(train[cols])
            clusters=DataFrame({'Time':train.Time,'Clusters':brc.predict(train[cols])})
            clusters=clusters[['Time','Clusters']]
            details=post_process(kpi_imputated_data,clusters)
            pred=brc.predict(test[cols])
            test_clusters=DataFrame({'Time':test.Time,'Clusters':pred})
            test_clusters=test_clusters[['Time','Clusters']]
            
            kpi_data=kpi_imputated_data[kpi_imputated_data.Time.isin(test_clusters.Time)]
            kpi_data.index=kpi_data.Time
            result=DataFrame()
            kpi_cols = [col for col in kpi_data.columns if col not in ['Time']]
            p=1
            for j in kpi_data.index:
                if(p%10==0):
                    print(str(int(p/len(kpi_data.index)*100))+'%')
                clus=int(test_clusters[test_clusters.Time==j].Clusters)
                base=details[['Mean_'+str(clus),'STD_'+str(clus)]]                
                for k in kpi_cols:
                    mean=base.loc[k,'Mean_'+str(clus)]
                    std=base.loc[k,'STD_'+str(clus)]
                    upper=mean+(3*std)
                    lower=mean-(3*std)
                    act=kpi_data[k][j]
                    if((lower < act < upper) | ((act==lower)&(act==upper))):
                        ano=0
                    else:
                        ano=1
                    dd=DataFrame({'Time':str(j),'Kpi':k,'Actual_Data':act,'Upper':upper,'Lower':lower,'Anomaly':ano},index=[0])
                    result=result.append(dd)
                p=p+1
            result=result[['Time','Kpi','Actual_Data','Upper','Lower','Anomaly']]
            result.index=range(0,len(result))
            return result
    
def post_process(kpi_imputated_data,clusters):
    if((len(kpi_imputated_data)>0) & (len(clusters)>0)):
        cls=clusters['Clusters'].unique()
        cls.sort()
        cols=[col for col in kpi_imputated_data.columns if col not in ['Time']]
        result=DataFrame(index=cols,columns=['{}_{}'.format(a, b) for b in cls for a in ['Mean','STD','Range']])
        for i in cls:
            times=list(clusters.loc[clusters.Clusters==i,'Time'])
            dd=kpi_imputated_data[kpi_imputated_data.Time.isin(times)]
            dd=dd[cols]
            result['Mean_'+str(i)]=dd.mean()
            result['STD_'+str(i)]=dd.std()
            result['Range_'+str(i)]=dd.max()-dd.min()
        return result
            
            
    
    
    