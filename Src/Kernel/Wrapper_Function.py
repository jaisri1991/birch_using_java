def java_fun(data_set,app_name):
    percentage= 1 - (1/round(len(data_set)/roll_apply))
    os.system('java -jar jars\spark-birch-1.0-SNAPSHOT.jar -m C -n '+str(percentage)+' -c 20 -d Data\\'+app_name+'\Raw_df.csv')
    return percentage

def scoring_wrapper(comp_info,app_name,percentage):
    #ano_data=df_anomalydef(comp_info,app_name,percentage,roll_apply)
    ano_data=anomalydef_scoring(comp_info,app_name,method,kpi_threshold,txn_threshold,pcl,percentage,roll_apply,ema_win_len,high,med,low)
    table_update(ano_data)
    return ano_data

def table_update(df):
    t0=time.process_time()
    conn=dbfun()
    try:
        cursor=conn.cursor()
        cols = [col for col in df.columns if col not in ['Time', 'CompInstanceId']]
        data = df.set_index(['Time'])[cols].to_records().tolist()
        dbquery = """insert into AD_RT_ANOMALYDEF values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE  Time=VALUES(Time),App_Id=VALUES(App_Id),Comp_Id=VALUES(Comp_Id),Layer=VALUES(Layer),Win_Len=VALUES(Win_Len), Comp_Name=VALUES(Comp_Name), Metric_Name=VALUES(Metric_Name), Anomaly=VALUES(Anomaly),Mean=VALUES(Mean), Lower=VALUES(Lower),Upper=VALUES(Upper),Actual_Value=VALUES(Actual_Value),STL_Value=VALUES(STL_Value),AD_Method=VALUES(AD_Method),Original_Score=VALUES(Original_Score),Anomaly_Scored=VALUES(Anomaly_Scored);"""
        try:
            cursor.executemany(dbquery,data)
            conn.commit()
        except:
            conn.rollback()
        print("Time Taken For Mysql Update Is %s"%(time.process_time()-t0))
    except Exception as e:
        print(e)
    finally:
        cursor.close()