def df_anomalydef(comp_info,app_name,percentage,roll_apply):
    files=os.listdir(Base_Dir+'/Data/'+app_name+'/output')
    files.remove('TXN_Clusters.csv')
    peak_data=DataFrame()
    for file in files:
        kpi=file.replace('.csv','')
        data=pd.read_csv(Base_Dir+'/Data/'+app_name+'/output'+'/'+file)
        data.Time = pd.to_datetime(data.Time, format='%d-%m-%Y %H:%M')
        dd=data[round(len(data)*percentage):]
        dd['Upper']=dd['Mean']+(3*dd['SD'])
        dd['Lower']=dd['Mean']-(3*dd['SD'])
        for i in range(0,len(comp_info)):
            if(comp_info.Name[i]==kpi[0:len(comp_info.Name[i])]):
                kpi_d=comp_info[comp_info.Name==comp_info.Name[i]]
                break
        if(kpi_d['Layer'].values[0] != 'TXN'):
            kpi_name=kpi.replace(kpi_d.Name.values[0]+'_','')
        df=DataFrame({'Time':dd.Time,'App_Id':[kpi_d.App_Id.values[0]]*len(dd),'Comp_Id':[kpi_d.CompId.values[0]]*len(dd),'Layer':[kpi_d.Layer.values[0]]*len(dd),'Win_Len':[roll_apply]*len(dd),'Comp_Name':[kpi_d.Name.values[0]]*len(dd),'Metric_Name':[kpi_name]*len(dd),'Anomaly':dd['Is3SDAnomaly'],'Mean':dd['Mean'],'Lower':dd['Lower'],'Upper':dd['Upper'],'Actual_Value':dd[kpi],'AD_Method':['M002']*len(dd),'STL_Value':dd['Cluster ID'],'Original_Score':[0]*len(dd),'Anomaly_Scored':dd['Is3SDAnomaly']})
        peak_data=peak_data.append(df)
    peak_data=peak_data[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','STL_Value','AD_Method','Original_Score','Anomaly_Scored']]
    return peak_data