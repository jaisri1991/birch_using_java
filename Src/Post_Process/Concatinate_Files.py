def concatenate(app_name,percentage):
    files=os.listdir(Base_Dir+'/Data/'+app_name+'/output')
    files.remove('TXN_Clusters.csv')
    peak_data=DataFrame()
    for file in files:
        kpi=file.replace('.csv','')
        data=pd.read_csv(Base_Dir+'/Data/'+app_name+'/output'+'/'+file)
        data.Time = pd.to_datetime(data.Time, format='%d-%m-%Y %H:%M')
        dd=data[round(len(data)*percentage):]
        sds=list(dd.filter(regex='Anomaly').columns.sort_values())
        if(len(sds)>2):
            dd=DataFrame({'Time':dd.Time,kpi+'_Low':dd[sds[0]],kpi+'_Medium':dd[sds[1]],kpi+'_High':dd[sds[2]]})
        elif(len(sds)>1):
            dd=DataFrame({'Time':dd.Time,kpi+'_Medium':dd[sds[0]],kpi+'_High':dd[sds[1]]})
        else:
            dd=DataFrame({'Time':dd.Time,kpi:dd[sds[0]]})
        if(len(dd)>0):
            if(file==files[0]):
                peak_data=dd
            else:
                peak_data=peak_data.merge(dd,on='Time',how='outer',sort=True)
    return peak_data