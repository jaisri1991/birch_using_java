def apply_weightage(data,high=None,med=None,low=None):
    high_cols=data.filter(regex='_High').columns
    med_cols=data.filter(regex='_Medium').columns
    low_cols=data.filter(regex='_Low').columns
    dd=data[:]
    #If the columns are applicable and if the values if set for high/med/low then those weightages will be applied to the dataframe
    if((len(high_cols)>0)&(not not high)):
        dd[high_cols]=dd[high_cols]*high
    if((len(med_cols)>0)&(not not med)):
        dd[med_cols]=dd[med_cols]*med
    if((len(low_cols)>0)&(not not low)):
        dd[low_cols]=dd[low_cols]*low
    if((len(high_cols)==0)&(len(med_cols)==0)&(len(low_cols)==0)):
        dd=dd*.75
    #Extracting Unique Column Names
    uni_cols=dd.rename(columns=lambda x: str(x).replace('_High', '').replace('_Medium', '').replace('_Low', '')).columns.unique()
    #Summing up all the kpis weightages as one column instead of three(high/med/low)
    for c in uni_cols:
        sum_value=DataFrame({c:dd.filter(regex=c).sum(axis=1)})
        if(c==uni_cols[0]):
            Result=sum_value
        else:
            Result[c]=sum_value
    Result.index=dd.index
    return Result
