def alert_count(app_id,app_name):
    max_layer=db_result('select count(distinct(Layer)) Count from AD_RT_ANOMALYDEF where App_Id='+str(app_id))
    max_layer=max_layer.Count[0]
    ind=db_result('select Time from AD_RT_ANOMALYDEF where App_Id='+str(app_id)+' group by DATE(Time),HOUR(Time) order by Time')
    data=db_result('select Time,count(distinct Layer)Layer_Count from AD_RT_ANOMALYDEF where App_Id='+str(app_id)+' and Anomaly_Scored>0 group by DATE(Time),HOUR(Time) order by Time')
    data=ind.merge(data,on='Time',how='outer',sort=True)
    data=data.fillna(0)
    data.Layer_Count=pd.to_numeric(data.Layer_Count,downcast='integer')
    dd=DataFrame({'Time':data.Time,'Low':0,'Med':0,'High':0,'None':0})
    val=round(max_layer/3)
    dd['None'][data[data.Layer_Count==0].index]=1
    low=data[(data.Layer_Count>0) & (data.Layer_Count <=(val))]
    dd['Low'][low.index]=low.Layer_Count
    med=data[(data.Layer_Count>val) & (data.Layer_Count <=(val*2))]
    dd['Med'][med.index]=med.Layer_Count
    high=data[(data.Layer_Count>(val*2))]
    dd['High'][high.index]=high.Layer_Count
    dd=dd[['Time','Low','Med','High','None']]
    dd.to_csv(app_name+'_Alert_Counts.csv',index=False)