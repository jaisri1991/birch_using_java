def plot_birch(kpi_imputated_data,result,app_name,roll_apply,percentage):
    cols=list(result.Kpi.unique())    
    for kpi in cols:
        act_data=kpi_imputated_data[['Time',kpi]]
        act_data.index=act_data.Time
        act_data.drop('Time',axis=1,inplace=True)
        data=result[result.Kpi==kpi]
        if not os.path.exists('Plots/Birch/'+app_name):
            os.makedirs('Plots/Birch/'+app_name)
        os.chdir('Plots/Birch/'+app_name)
        data.index=pd.to_datetime(data.Time)
        axvl=data.index[0]
        plt.figure(figsize=(12,10))
        x = data['Actual_Data']
        plt.plot(act_data,color='orange')
        high=data[(data['Anomaly']>0)].index
        plt.plot(x[high],'ro',color='r')
        plt.axvline(x=axvl,color='r')
        plt.xticks(rotation=45)
        plt.title(str(kpi)+'_'+str(roll_apply)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(kpi)+'_'+str(roll_apply)+'_Win_len and '+str(percentage)+'_Perc.jpg')
        os.chdir('../../..')

def db_result(query):
    conn=dbfun()
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        df=pd.DataFrame(rows)
        if(len(df)>0):
            df.columns=[i[0] for i in cursor.description]
            print('Total Row(s):', cursor.rowcount)
        return df
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def comp_txn(app_list,app_name):
    app=app_list[app_list.App_Name==app_name]
    comp_list=app.Comp_List.values[0]
    txn_list=app.Txn_List.values[0]
    app_id=app.App_Id.values[0]
    return app_id,comp_list,txn_list


def calcInputDateRange(app_id):
    sql_string = 'select max(Time) as Time from AD_RT_ANOMALYDEF where App_Id = ' + str(app_id)
    time_df=db_result(sql_string)
    if(time_df.Time[0]!=None):
        #Last refresh date
        last_refreshed = time_df['Time'].min().to_pydatetime()
        return last_refreshed
    else:
        return default_to

def non_overlapping_average(raw_df,window_len,aggr_method='mean'):
    averaged_df=DataFrame()
    if((isinstance(raw_df.index,pd.core.indexes.datetimes.DatetimeIndex))):
        if (aggr_method == 'mean'):
            averaged_df = raw_df.resample(str(window_len)+'Min').mean()
        elif (aggr_method == 'median'):
            averaged_df = raw_df.resample(str(window_len)+'Min').median()
        elif (aggr_method == 'skew'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.skew)
        elif (aggr_method == 'kurt'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.kurt)
    else:
        print('DataFrame Index is not DateTime')
    return averaged_df

def impute_using_sma(x,cols,n):
    #t1 = time.time()
    #print('Imputation Process Started')
    if(isinstance(cols,list)):
        for f in cols:
          #print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    #print("Total Time Taken For Imputation Process: %s" % (time.time() - t1))
    return x

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isnull(x.ix[0,f]) == True):
        nn = x[f].notnull().nonzero()[0]
        if (len(nn) > 1):
            x.ix[0, f]=x.ix[nn[0]:nn[1]+1, f].mean()
        else:
            x.ix[0, f] =0
    if (count > n):
        ### Method is SMA
        row_no = pd.isnull(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if((len(row_no)==count-1)&(x.ix[0, f]==0)):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()