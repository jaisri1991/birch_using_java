#Default variables
hostip='192.168.13.83'
default_frm='2017-10-01 00:00:00'
default_to='2017-11-30 23:59:00'
method='PC'
pcl=95
roll_apply=60
ema_win_len=3
txn_threshold=.25
kpi_threshold=.25
high=0.75
med=0.25
low=None
app_list=pd.read_csv(Base_Dir+'/config/app_list.csv')